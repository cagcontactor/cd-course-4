package com.caglabs.goldenzone.helloservice.impl;

import com.caglabs.goldenzone.helloservice.client.Hello;
import com.caglabs.goldenzone.infomodel.entity.GoldenzoneUser;
import com.caglabs.goldenzone.infomodel.entity.GoldenzoneUserDao;
import org.jmock.Expectations;
import org.jmock.Mockery;
import org.jmock.lib.legacy.ClassImposteriser;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.util.ReflectionTestUtils;

import static org.junit.Assert.assertTrue;

public class HelloBeanTest {
    private Mockery mockery = new Mockery() {{
        setImposteriser(ClassImposteriser.INSTANCE);
    }};
    private HelloBean helloBean;
    private GoldenzoneUserDao goldenzoneUserDao;

    @Before
    public void setup() {
        goldenzoneUserDao = mockery.mock(GoldenzoneUserDao.class);
        helloBean = new HelloBean();
        ReflectionTestUtils.setField(helloBean, "goldenzoneUserDao", goldenzoneUserDao);
    }

    @Test
    public void shouldSayHello() throws Hello.HelloException {
        final GoldenzoneUser bob = new GoldenzoneUser("bob", "Bob Banana");
        ReflectionTestUtils.setField(bob, "id", 42L);
        mockery.checking(new Expectations() {{
            oneOf(goldenzoneUserDao).getSystemUserByName("bob");
            will(returnValue(bob));
        }});
        String helloString = helloBean.sayHello("bob");
        assertTrue(helloString,
                helloString.matches("Hello Bob Banana \\(id=42\\)!!! The time is [0-9]{2}:[0-9]{2}:[0-9]{2}"));
    }
}