Test
====
This module contains test drivers (automated tests) for different test types:
- BDD
- Service Integration
- Performance
- System Integration (UI + services)

However, unit tests are not found here. They are of course included in the each module.

It also contains different tools and client proxies to be used by test drivers.