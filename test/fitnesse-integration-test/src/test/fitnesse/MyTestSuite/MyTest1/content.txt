!contents -R2 -g -p -f -h

Test that we can set values and check results.

!| myFixtures.RightTriangle|
|a   |b   |c    |right()?  |
|3   |4   |5    |true      |
|6   |8   |10   |true      |
|3   |5   |9    |false     |
|4   |4   |4    |false    |
