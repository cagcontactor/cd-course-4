## !contents -R2 -g -p -f -h

Use the tags property to differentiate among test cases. Tags can be such as smoke, critical, nightly... 

Try it. Set smoke on a few to run only those.

Some hints are here: .FitNesse.UserGuide.TestSuites.TagsAndFilters