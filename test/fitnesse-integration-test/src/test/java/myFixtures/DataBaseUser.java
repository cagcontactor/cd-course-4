/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package myFixtures;

import com.caglabs.goldenzone.helloclient.HelloBean;
import com.caglabs.goldenzone.helloclient.HelloException_Exception;
import com.caglabs.goldenzone.helloclient.HelloServiceLocator;

/**
 *
 * @author matsl
 */
public class DataBaseUser {

    private final String name;
    private final String realName;
    
    HelloBean hello = new HelloServiceLocator().getService();
    
    public DataBaseUser(String name, String realname) {
        this.name = name;
        this.realName = realname;
    }

    public String sayHello() {
        try {
            System.out.println("sayHello('" + name + "')");
            return hello.sayHello(name);
        } catch (HelloException_Exception e) {
            System.out.println("Caught exception: " + e.getMessage());
            // e.printStackTrace(System.out);
            return "Exception";
        }
    }

    public boolean addUser() {
        try {
            System.out.println("adduser('" + name + ", " + realName + "')");
            hello.addUser(name, realName);
            return true;
        } catch (HelloException_Exception e) {
            System.out.println("Caught exception: " + e.getMessage());
            // e.printStackTrace(System.out);
            return false;
        }
    }

    public boolean removeUser() {
        try {
            System.out.println("removeUser('" + name + "')");
            hello.removeUser(name);
            return true;
        } catch (HelloException_Exception e) {
            System.out.println("Caught exception: " + e.getMessage());
            // e.printStackTrace(System.out);
            return false;
        }
    }

    public void updateUser() {
        try {
            System.out.println("updateUser('" + name + ", " + realName + "')");
            hello.updateUser(name, realName);
        } catch (HelloException_Exception e) {
            System.out.println("Caught exception: " + e.getMessage());
            // e.printStackTrace(System.out);
        }
    }

}
