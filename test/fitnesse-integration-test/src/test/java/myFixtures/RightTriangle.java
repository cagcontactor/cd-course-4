package myFixtures;

public class RightTriangle {

    private double a;
    private double b;
    private double c;

    public boolean right() {
        if (a == 4) {
            return false;
        }
        return (a * a + b * b) == (c * c);
    }

    // Fixture SLIM
    public void setA(double a) {
        this.a = a;
    }

    public void setB(double b) {
        this.b = b;
    }

    public void setC(double c) {
        this.c = c;
    }
}
