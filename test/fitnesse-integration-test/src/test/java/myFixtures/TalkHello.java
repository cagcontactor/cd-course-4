/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package myFixtures;

import com.caglabs.goldenzone.helloclient.HelloBean;
import com.caglabs.goldenzone.helloclient.HelloException_Exception;
import com.caglabs.goldenzone.helloclient.HelloServiceLocator;

/**
 *
 * @author matsl
 */
public class TalkHello {

    public String name;
    public String realName;

    public void setName(String name) {
        this.name = name;
    }

    public void setRealName(String realName) {
        this.realName = realName;
    }

    public String sayHello() {
        HelloBean hello = new HelloServiceLocator().getService();
        try {
            System.out.println("sayHello('" + name + "')");
            return hello.sayHello(name);
        } catch (HelloException_Exception e) {
            System.out.println("Caught exception: " + e.getMessage());
            // e.printStackTrace(System.out);
            return "Exception";
        }
    }

    public void addUser() {
        HelloBean hello = new HelloServiceLocator().getService();
        try {
            System.out.println("adduser('" + name + ", " + realName + "')");
            hello.addUser(name, realName);
        } catch (HelloException_Exception e) {
            System.out.println("Caught exception: " + e.getMessage());
            // e.printStackTrace(System.out);
        }
    }

    public void removeUser() {
        HelloBean hello = new HelloServiceLocator().getService();
        try {
            System.out.println("removeUser('" + name + "')");
            hello.removeUser(name);
        } catch (HelloException_Exception e) {
            System.out.println("Caught exception: " + e.getMessage());
            // e.printStackTrace(System.out);
        }
    }

    public void updateUser() {
        HelloBean hello = new HelloServiceLocator().getService();
        try {
            System.out.println("updateUser('" + name + ", " + realName + "')");
            hello.updateUser(name, realName);
        } catch (HelloException_Exception e) {
            System.out.println("Caught exception: " + e.getMessage());
            // e.printStackTrace(System.out);
        }
    }

}
