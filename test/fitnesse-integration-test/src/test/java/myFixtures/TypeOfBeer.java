package myFixtures;

import myFixtures.exceptions.UnknownBeerException;

public class TypeOfBeer {
	
	public TypeOfBeer() {
		// TODO Auto-generated constructor stub
	}

	private String beer;

	public String getBeer() {
		return beer;
	}

	public void setBeer(String beer) {
		this.beer = beer;
	}

	public String type() {
		try {
			return Beers.fromDisplayName(beer).type();
		} catch (UnknownBeerException e) {
			// TODO Auto-generated catch block
			return e.getClass().getSimpleName();
		}
	}
	public boolean hipster(){
		try {
			return Beers.fromDisplayName(beer).isHipster();
		} catch (UnknownBeerException e) {
			// TODO Auto-generated catch block
			return false;
		}
	}

	private enum Beers {
		Budweiser("Budweiser", "lager",false),
		Hof("Hof", "lager", false),
		DalesPaleAle("Dales Pale Ale", "IPA", true),
		PabstBlueRibbon("Pabst Blue Ribbon", "lager", true),
		Guiness("Guiness", "Stout", false);
		private String display;
		String type;
		private boolean hipster;
		private Beers(String displayName, String type, boolean isHipster) {
			this.display=displayName;
			this.type=type;
			this.hipster=isHipster;
		}
		public static Beers fromDisplayName(String displayName) throws UnknownBeerException{
			for (Beers b : Beers.values()) {
				if (b.getDisplay().equals(displayName))
					return b;
			}
			throw new UnknownBeerException();
		}
		public String type(){
			return this.type;
		}
		public String getDisplay() {
			return display;
		}
		public boolean isHipster() {
			return hipster;
		}
	}
}
