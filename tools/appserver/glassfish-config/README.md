Glassfish Configurations
========================
This module defines different Glassfish configurations used in other modules using embedded glassfish.

The configurations are available as zip files and can be included as follows:

    <properties>
        <!--
             Available configurations:
             dev=Development tests
             it=Integration tests
             prod=Production
        -->
        <target-environment>dev</target-environment>
    </properties>

    <build>
        <plugins>

            <plugin>
                <groupId>org.glassfish.embedded</groupId>
                <artifactId>maven-embedded-glassfish-plugin</artifactId>
                <version>3.1.2.2</version>
                <configuration>
                    <configFile>${project.build.directory}/dependency/${target-environment}-domain.xml</configFile>
                    <app>${project.build.directory}/dependency/app-ear-${project.version}.ear</app>
                    <containerType>ear</containerType>
                    <name>goldenzone</name>
                </configuration>
                <dependencies>
                    <dependency>
                        <groupId>mysql</groupId>
                        <artifactId>mysql-connector-java</artifactId>
                        <version>5.1.19</version>
                    </dependency>
                </dependencies>
            </plugin>

            <plugin>
                <artifactId>maven-dependency-plugin</artifactId>
                <executions>
                    <execution>
                        <id>unpack</id>
                        <phase>process-test-resources</phase>
                        <goals>
                            <goal>unpack</goal>
                        </goals>
                        <configuration>
                            <artifactItems>
                                <artifactItem>
                                    <groupId>${project.parent.groupId}</groupId>
                                    <artifactId>glassfish-config</artifactId>
                                    <version>${project.parent.version}</version>
                                    <classifier>target-environment></classifier>
                                    <type>zip</type>
                                </artifactItem>
                            </artifactItems>
                        </configuration>
                    </execution>

                    <execution>
                        <id>copy</id>
                        <phase>process-test-resources</phase>
                        <goals>
                            <goal>copy</goal>
                        </goals>
                        <configuration>
                            <artifactItems>
                                <artifactItem>
                                    <groupId>${project.parent.groupId}</groupId>
                                    <artifactId>app-ear</artifactId>
                                    <version>${project.parent.version}</version>
                                    <type>ear</type>
                                </artifactItem>
                            </artifactItems>
                        </configuration>
                    </execution>

                </executions>
            </plugin>

        </plugins>
    </build>
