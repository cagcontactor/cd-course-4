copy-soapui-groovy-dependencies
===============================
Groovy code in SoapUI may call Java code. Dependencies
are listed in the depencencies section within the configuration for soapui-pro-maven-plugin in the module
soapui-integration-test.

This module, copy-soapui-groovy-dependencies, is used to find dependency jars for standalone SoapUI Pro GUI in order
to be able to run the same project that is runnning in soapui-integration-test.

Usage
=====
Run
   $ mvn install
in this directory.

The command creates jar files in the directory target/jars_to_copy_to_soapui_lib_ext. Copy all these jar files
to your SoapUI Pro installation directory below bin/ext. Then restart SoapUI Pro.
