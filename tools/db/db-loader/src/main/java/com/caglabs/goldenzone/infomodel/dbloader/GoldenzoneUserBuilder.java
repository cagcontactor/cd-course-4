/*
 * Created by Daniel Marell 13-10-22 9:28 PM
 */
package com.caglabs.goldenzone.infomodel.dbloader;

import com.caglabs.goldenzone.infomodel.entity.GoldenzoneUser;

public class GoldenzoneUserBuilder {
    private String name = "Kalle";
    private String displayName = "Kalle Kula";

    public GoldenzoneUserBuilder() {
    }

    public GoldenzoneUserBuilder(String name, String displayName) {
        this.name = name;
        this.displayName = displayName;
    }

    public GoldenzoneUserBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public GoldenzoneUserBuilder withDisplayName(String displayName) {
        this.displayName = displayName;
        return this;
    }

    public GoldenzoneUser build() {
        return new GoldenzoneUser(name, displayName);
    }
}
